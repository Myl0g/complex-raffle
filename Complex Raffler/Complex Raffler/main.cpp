//
//  main.cpp
//  Complex Raffler
//
//  Created by Milo Gilad on 7/8/16.
//  Copyright © 2016 Milo Gilad. All rights reserved.
//

#include "Utilities.h"

using namespace std;

string none = "Placeholder";

void raffler()
{
    
}

void instructions()
{
    cout << none << endl;
}

void welcomeScreen()
{
    int option;
    cout << "Welcome to the complex raffler." << endl;
    cout << "Press '1' and then enter to see the instructions." << endl;
    cout << "Press '2' and then enter to begin." << endl;
    cout << "> ";
    cin >> option;
    if(option == 2)
    {
        raffler();
    }
    else
    {
        instructions();
    }
}

int main()
{
    welcomeScreen();
}